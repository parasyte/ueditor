/**
 * uEditor  2012 Jason Oster
 *
 * Based on TinyEditor by Michael Leigeber.
 * http://www.scriptiny.com/2010/02/javascript-wysiwyg-editor/
 *
 * Uses Silk icons set By Mark James.
 * http://www.famfamfam.com/lab/icons/silk/
 *
 * Originally released under the terms of CC-BY-3.0
 * See cc-by-3.0.txt for full license information.
 */

$u = (function u() {
    var createElement = function createElement(type) {
            return document.createElement(type);
        },
        is_ie = !!document.all,
        is_webkit = /webkit/i.test(navigator.userAgent),
        is_firefox = /firefox/i.test(navigator.userAgent),
        commands = {
            "bold"          : [1, "Bold", "a", "bold"],
            "italic"        : [2, "Italic", "a", "italic"],
            "underline"     : [3, "Underline", "a", "underline"],
            "strikethrough" : [4, "Strike-through", "a", "strikethrough"],
            "subscript"     : [5, "Subscript", "a", "subscript"],
            "superscript"   : [6, "Superscript", "a", "superscript"],
            "orderedlist"   : [7, "Insert Ordered List", "a", "insertorderedlist"],
            "unorderedlist" : [8, "Insert Unordered List", "a", "insertunorderedlist"],
            "outdent"       : [9, "Outdent", "a", "outdent"],
            "indent"        : [10, "Indent", "a", "indent"],
            "leftalign"     : [11, "Left Align", "a", "justifyleft"],
            "centeralign"   : [12, "Center Align", "a", "justifycenter"],
            "rightalign"    : [13, "Right Align", "a", "justifyright"],
            "blockjustify"  : [14, "Block Justify", "a", "justifyfull"],
            "undo"          : [15, "Undo", "a", "undo"],
            "redo"          : [16, "Redo", "a", "redo"],
            "image"         : [17, "Insert Image", "i", "insertimage", "Enter Image URL : ", "http://"],
            "hr"            : [18, "Insert Horizontal Rule", "a", "inserthorizontalrule"],
            "link"          : [19, "Insert Hyperlink", "i", "createlink", "Enter URL : ", "http://"],
            "unlink"        : [20, "Remove Hyperlink", "a", "unlink"],
            "unformat"      : [21, "Remove Formatting", "a", "removeformat"],
            "print"         : [22, "Print", "a", "print"]
        },
        shortcuts = [
            [ 66, "bold" ],
            [ 73, "italic" ],
            [ 74, "insertorderedlist" ],
            [ 75, "insertunorderedlist" ],
            [ 85, "underline" ],
            [ 89, "redo" ],
            [ 90, "undo" ],
        ],
        controls = [
            "bold", "italic", "underline", "strikethrough", "|",
            "subscript", "superscript", "|",
            "orderedlist", "unorderedlist", "|",
            "outdent", "indent", "|",
            "leftalign", "centeralign", "rightalign", "blockjustify", "|",
            "unformat", "|",
            "undo", "redo",
            "\n",
            "font", "size", "style", "|",
            "image", "hr", "link", "unlink", "|",
            "print"
        ],
        MODE_TEXT = 0,
        MODE_WYSIWYG = 1;


    function css(node, prop) {
        return window.getComputedStyle ?
            window.getComputedStyle(node, null).getPropertyValue(prop) :
            node.currentStyle[prop];
    }

    function addClass(node, cls) {
        node.className += " " + cls;
    }

    /**
     * Helper function to get the bounding rectangle for an element.
     * @private
     * @function
     */
    function getBB(node) {
        return node.getBoundingClientRect();
    }

    /**
     * Helper function to get the selected value from a dropdown
     * @private
     * @function
     */
    function getValue(node) {
        return node.options[node.selectedIndex].value;
    }

    /**
     * Shim to support Function.prototype.bind
     * @private
     * @function
     */
    if (!Function.prototype.bind) {
        Function.prototype.bind = function bind(self) {
            return function bound() {
                return self.apply(self, Array.prototype.slice.call(arguments));
            };
        };
    }

    /**
     * Editor constructor
     * @public
     * @function
     * @param {Object} obj A commandsuration object
     * @example
     * var editor = new $u.edit(id, {
     *     // controls you want available; "|" for a divider, "\n" for a new row
     *     "controls"   : [
     *         "bold", "italic", "underline", "strikethrough", "|",
     *         "subscript", "superscript", "|",
     *         "orderedlist", "unorderedlist", "|",
     *         "outdent", "indent", "|",
     *         "leftalign", "centeralign", "rightalign", "blockjustify", "|",
     *         "unformat", "|",
     *         "undo", "redo",
     *         "\n",
     *         "font", "size", "style", "|",
     *         "image", "hr", "link", "unlink", "|",
     *         "print"
     *     ],
     *
     *     // array of fonts to display
     *     "fonts"      : [
     *         "Times",
     *         "Verdana",
     *         "Arial",
     *         "Georgia",
     *         "Courier New",
     *         "Trebuchet MS"
     *     ],
     *
     *     // array of font sizes
     *     "sizes"      : [ 1, 2, 3, 4, 5, 6, 7 ],
     *
     *     // attach an external CSS file to the editor
     *     "css"        : "style.css",
     *
     *     // show the footer (default = false)
     *     "footer"     : {
     *         // enable resize (default = false)
     *         "resize"     : true,
     *
     *         // enable wysisyg and source mode toggle (default = false)
     *         "toggle"     : {
     *             "text"       : "source",
     *             "activetext" : "wysiwyg"
     *         }
     *     },
     *
     *     // focus the editor on load (default = false)
     *     "focus"      : true
     * });
     */
    function edit(id, obj) {
        this.el = document.getElementById(id);
        this.obj = obj || {};
        this.mode = MODE_WYSIWYG;

        var self = this,
            ueditor = createElement("div"),
            header,
            c = [ "\n" ].concat(this.obj.controls || controls),
            l = c.length,
            i = 0;

        this.wrapper = createElement("div");
        this.iframe = createElement("iframe");

        ueditor.className = "ueditor";

        for (; i < l; i++) {
            id = c[i];

            if (id == "\n") {
                header = createElement("div");
                header.className = "ueditor-header ueditor-user-select";
                ueditor.appendChild(header);
            }
            else if (id == "|") {
                var divider = createElement("div");
                divider.className = "ueditor-divider";
                header.appendChild(divider);
            }
            else if (id == "font") {
                var sel = createElement("select"),
                    fonts = this.obj.fonts || [ "Times", "Helvetica", "Courier New" ],
                    fl = fonts.length,
                    x = 0;

                sel.className = "ueditor-font";
                sel.onchange = function onchange(e) {
                    self.saveSelection();
                    self.action("fontname", getValue(this));
                };
                sel.options[0] = new Option("Font", "");
                sel.options[0].disabled = true;
                sel.options[0].selected = true;

                for (; x < fl; x++) {
                    var font = fonts[x];
                    sel.options[x + 1] = new Option(font, font);
                }
                this.font = sel;
                header.appendChild(sel);
            }
            else if (id == "size") {
                var sel = createElement("select"),
                    sizes = this.obj.sizes || [ 1, 2, 3, 4, 5, 6, 7 ],
                    sl = sizes.length,
                    x = 0;

                sel.className = "ueditor-size";
                sel.onchange = function onchange(e) {
                    self.saveSelection();
                    self.action("fontsize", getValue(this));
                };

                sel.options[0] = new Option("Size", "");
                sel.options[0].disabled = true;
                sel.options[0].selected = true;

                for (; x < sl; x++) {
                    var size = sizes[x];
                    sel.options[x + 1] = new Option(size, size);
                }
                this.size = sel;
                header.appendChild(sel);
            }
            else if (id == "style") {
                var sel = createElement("select"),
                    styles = this.obj.styles || [
                        [ "Style", "" ],
                        [ "Paragraph", "<p>" ],
                        [ "Header 1", "<h1>" ],
                        [ "Header 2", "<h2>" ],
                        [ "Header 3", "<h3>" ],
                        [ "Header 4", "<h4>" ],
                        [ "Header 5", "<h5>" ],
                        [ "Header 6", "<h6>" ]
                    ],
                    sl = styles.length,
                    x = 0;

                sel.className = "ueditor-style";
                sel.onchange = function onchange(e) {
                    self.saveSelection();
                    self.action("formatblock", getValue(this));
                };

                for (; x < sl; x++) {
                    var style = styles[x];
                    sel.options[x] = new Option(style[0], style[1]);
                }
                sel.options[0].disabled = true;
                sel.options[0].selected = true;
                this.style = sel;
                header.appendChild(sel);
            }
            else if (commands[id]) {
                var div = createElement("div"),
                    icon = createElement("div"),
                    args = commands[id],
                    pos = args[0] * -16;

                div.className = "ueditor-control";
                div.title = args[1];

                icon.className = "ueditor-icon";
                icon.style.backgroundPosition = "0 " + pos + "px";
                div.appendChild(icon);

                div.onmousedown = (function handlerFactory(args) {
                    return function onmousedown(e) {
                        self[args[2] == "a" ? "action" : "insert"].apply(self, args.slice(3));
                    }
                })(args);

                header.appendChild(div);
                if (is_ie) {
                    div.unselectable = "on";
                }
            }
        }

        this.wrapper.style.width = css(this.el, "width");
        this.wrapper.style.height = css(this.el, "height");

        this.el.parentNode.insertBefore(ueditor, this.el);
        this.el.style.display = "none";

        this.wrapper.appendChild(this.el);
        this.wrapper.appendChild(this.iframe);
        ueditor.appendChild(this.wrapper);

        if (this.obj.footer) {
            var footer = createElement("div");
            footer.className = "ueditor-footer ueditor-user-select";

            if (this.obj.footer.toggle) {
                var ts = createElement("div");

                ts.className = "ueditor-toggle";
                ts.innerHTML = this.obj.footer.toggle.text || "source";
                ts.onclick = function onclick(e) {
                    self.toggle(this);
                    return false;
                };
                this.toggler = ts;
                footer.appendChild(ts);
            }
            if (this.obj.footer.resize) {
                var rs = createElement("div");

                rs.className = "ueditor-resize";
                rs.onmousedown = function onmousedown(e) {
                    resize.bind(self)(e);
                    return false;
                };
                footer.appendChild(rs);
            }
            ueditor.appendChild(footer);
            is_ie && (footer.unselectable = "on");
        }


        this.window = this.iframe.contentWindow;
        this.document = this.iframe.contentWindow.document;
        this.document.open();

        var content = '<html style="height:100%"><head>';
        if (this.obj.css) {
            content += '<link rel="stylesheet" href="' + this.obj.css + '">';
        }
        content += '</head><body style="height:100%;margin:0" contenteditable="true">';
        content += (this.el.value || "<p><br></p>");
        content += "</body></html>";

        this.document.write(content);
        this.document.close();
        this.document.designMode = "on";

        if (this.obj.focus) {
            this.focus();
        }

        // Editor magic.
        function deferKeydown(e) {
            setTimeout(function defer() {
                updateDropdowns.bind(self)();
                cleanup.bind(self)();
            }, 1);

            return keydown.bind(self)(e);
        }

        function deferUpdate(e) {
            setTimeout(function defer() {
                updateDropdowns.bind(self)();
            }, 1);
        }

        // Firefox refuses to play nicely with DOM Level 2 events
        // when designMode is enabled on an iframe.
        if (is_firefox) {
            this.document.addEventListener("keydown", deferKeydown, true);
            this.document.addEventListener("mousedown", deferUpdate, true);
        }
        else {
            this.document.body.onkeydown = deferKeydown;
            this.document.body.onmousedown = deferUpdate;
        }
    }


    /* Private methods */
    function resize(e) {
        if (this.fr) {
            this.fr();
        }

        this.wrapper.offsetY = e.clientY - getBB(this.wrapper).bottom;
        this.fr = free.bind(this);

        this.mask = createElement("div");
        this.mask.className = "ueditor-mask";
        document.body.appendChild(this.mask);

        this.mask.onmousemove = move.bind(this);
        this.mask.onmouseup = this.mask.onmouseout = this.fr;
    }

    function move(e) {
        var y = e.clientY,
            h = y - this.wrapper.offsetY - getBB(this.wrapper).top;

        if (y >= 0) {
            this.wrapper.style.height = Math.max(h, 25) + "px";
        }
    }

    function free(e) {
        this.mask.onmousemove = this.mask.onmouseup = this.mask.onmouseout = null;
        document.body.removeChild(this.mask);
        delete this.mask;
        delete this.fr;
    }

    function updateDropdowns() {
        this.saveSelection();
        if (this.wysiwyg_range) {
            var self = this,
                node = this.wysiwyg_range.startContainer.parentNode,
                found = {
                    "font" : false,
                    "size" : false,
                    "style" : false
                }

            function findAttr(node, attr) {
                var value;
                while (node && (node.nodeName != "BODY")) {
                    if (value = node.getAttribute(attr)) {
                        return value;
                    }
                    node = node.parentNode;
                }
            }

            function walkDOM(node, callback) {
                while (node) {
                    callback(node, node.nodeName);
                    node = node.parentNode;
                }
            }

            walkDOM(node, function walker(node, name) {
                if (name == "FONT") {
                    var font = findAttr(node, "face"),
                        size = findAttr(node, "size");

                    function search(options, value, key) {
                        var l = options.length,
                            i = 0;

                        while (!found[key] && value && (i++ < l)) {
                            if (options[i].value == value) {
                                found[key] = true;
                                options[i].selected = true;
                                break;
                            }
                        }
                    }
                    self.font && search(self.font.options, font, "font");
                    self.size && search(self.size.options, size, "size");

                }
                else if (/^(P|H[1-6])$/.test(name)) {
                    var s = self.style;
                    if (!found.style && s) {
                        var value = "<" + name.toLowerCase() + ">",
                            sl = s.options.length,
                            i = 0;

                        for (; i < sl; i++) {
                            if (s.options[i].value == value) {
                                found.style = true;
                                s.options[i].selected = true;
                                break;
                            }
                        }
                    }
                }
            });

            found.font || (self.font.options[0].selected = true);
            found.size || (self.size.options[0].selected = true);
            found.style || (self.style.options[0].selected = true);
        }
    }

    function keydown(e) {
        // Process keyboard shortcuts.
        e = e || window.event;
        if (e.metaKey || e.ctrlKey) {
            var keyCode = e.keyCode || e.which,
                sl = shortcuts.length,
                i = 0,
                action;

            for (; i < sl; i++) {
                if (keyCode == shortcuts[i][0]) {
                    action = shortcuts[i][1];

                    if (e.shiftKey) {
                        action = (action == "undo") ? "redo" :
                                 (action == "redo") ? "undo" : action;
                    }
                    this.action(action);
                    break;
                }
            }

            if (action) {
                e.preventDefault && e.preventDefault();
                e.stopPropagation && e.stopPropagation();
                e.returnValue = false;
                e.cancelBubble = true;
                return false;
            }
        }
        return true;
    }

    function cleanup() {
        var rewrite = this.document.body.innerHTML,
            value = rewrite;

        // Always create paragraphs by default.
        if (!value || (/^<br ?\/?>$/.test(value))) {
            value = "<p><br></p>";
        }

        if (is_webkit) {
            value = value.replace(/<p>(<[ou]l>)/gi, "$1")
                .replace(/(<\/[ou]l>)<\/p>/gi, "$1")
                .replace(/<div><br ?\/?><\/div>/gi, '<p id="p_sel"><br></p>')
                .replace(/<li><br ?\/?><\/li>/i, '<li id="li_sel"><br></li>')
        };

        if (rewrite != value) {
            this.document.body.innerHTML = value;

            // Reset the caret position; setting innerHTML wipes it out.
            var node = this.document.getElementById("p_sel") ||
                this.document.getElementById("li_sel");
            if (node) {
                node.removeAttribute("id");
                this.setSelection(this.window, this.document, node);
            }
        }
    }

    function getSelection(win, doc) {
        return win.getSelection() || doc.getSelection();
    }


    /* Public methods */
    edit.prototype.focus = function focus() {
        var node = (!this.mode ? this.el : is_firefox ? this.iframe : this.document.body);

        setTimeout(function deferFocus() {
            node.focus();
        }, 1);
    }

    edit.prototype.saveSelection = function saveSelection() {
        function _getSelection(win, doc) {
            var sel = getSelection(win, doc);
            return sel.rangeCount ? sel.getRangeAt(0) : null;
        }

        if (document.getSelection) {
            this.wysiwyg_range = _getSelection(this.window, this.document);
            this.text_range = _getSelection(window, document);
        }
    };

    edit.prototype.restoreSelection = function restoreSelection() {
        function _restoreSelection(win, doc, range) {
            if (range) {
                var sel = getSelection(win, doc);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }

        _restoreSelection(this.window, this.document, this.wysiwyg_range);
        _restoreSelection(window, document, this.text_range);
    };

    edit.prototype.setSelection = function setSelection(win, doc, node) {
        if (doc.getSelection) {
            var sel = getSelection(win, doc);
            sel.removeAllRanges();
            sel.extend(node, 0);
        }
    };

    edit.prototype.action = function action(cmd, val) {
        this.restoreSelection();
        if (this.mode) {
            this.document.execCommand(cmd, 0, val || 0);
            cleanup.bind(this)();
        }

        this.focus();
        this.saveSelection();
    };

    edit.prototype.insert = function insert(cmd, msg, entry) {
        var val = prompt(msg, entry);
        if (val) {
            this.document.execCommand(cmd, 0, val);
        }
    };

    edit.prototype.toggle = function toggle(button) {
        if (this.mode) {
            this.el.value = this.document.body.innerHTML;

            if (button) {
                button.innerHTML = this.obj.footer.toggle.activetext || "wysiwyg"
                this.iframe.style.display = "none";
                this.el.style.display = "block";
                this.mode = MODE_TEXT;
            }
        }
        else {
            this.document.body.innerHTML = this.el.value || "<p><br></p>";

            if (button) {
                button.innerHTML = this.obj.footer.toggle.text || "source";
                this.el.style.display = "none";
                this.iframe.style.display = "block";
                this.mode = MODE_WYSIWYG;
            }
        }
    };

    edit.prototype.post = function post() {
        this.el.value = this.document.body.innerHTML;
    };

    // Return public API.
    return {
        "edit" : edit
    };
})();
