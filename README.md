uEditor
=======

A WYSIWYG HTML micro-Editor
---------------------------

Getting
-------

    $ git clone http://bitbucket.org/parasyte/ueditor.git

License
-------

uEditor is based on TinyEditor by Michael Leigeber.

> <http://www.scriptiny.com/2010/02/javascript-wysiwyg-editor/>

Uses Silk icons set By Mark James.

> <http://www.famfamfam.com/lab/icons/silk/>

Originally released under the terms of CC-BY-3.0
See [cc-by-3.0.txt](cc-by-3.0.txt) for full license information.

Using
-----

Starting with a textarea in your HTML, create the uEditor widget with a small
piece of JavaScript:

    <textarea id="ueditor">Hello world!</textarea>
    <script>
        var editor = new $u.edit("ueditor");
    </script>

Simply pass the textarea id as the first parameter to the constructor. See
[index.html](index.html) for a slightly larger example.

Options
-------

Many options are available to configure uEditor to your liking. They are passed
to the constructor as an object in the second parameter.

    var editor = new $u.edit(id, {
        // controls you want available; "|" for a divider, "\n" for a new row
        "controls"   : [
            "bold", "italic", "underline", "strikethrough", "|",
            "subscript", "superscript", "|",
            "orderedlist", "unorderedlist", "|",
            "outdent", "indent", "|",
            "leftalign", "centeralign", "rightalign", "blockjustify", "|",
            "unformat", "|",
            "undo", "redo",
            "\n",
            "font", "size", "style", "|",
            "image", "hr", "link", "unlink", "|",
            "print"
        ],

        // array of fonts to display
        "fonts"      : [
            "Times",
            "Verdana",
            "Arial",
            "Georgia",
            "Courier New",
            "Trebuchet MS"
        ],

        // array of font sizes
        "sizes"      : [ 1, 2, 3, 4, 5, 6, 7 ],

        // attach an external CSS file to the editor
        "css"        : "style.css",

        // show the footer (default = false)
        "footer"     : {
            // enable resize (default = false)
            "resize"     : true,

            // enable wysisyg and source mode toggle (default = false)
            "toggle"     : {
                "text"       : "source",
                "activetext" : "wysiwyg"
            }
        },

        // focus the editor on load (default = false)
        "focus"      : true
    });


API
---

The API provides some common functionality for reading, writing, and modifying
the editor content. Each method is available through the object returned by the
constructor. We'll call it `editor`.


### Methods

#### editor.focus()

> Give focus to the editor.


#### editor.saveSelection()

> Save the currently selected text range to the `editor.wysiwyg_range` and
> `editor.text_range` properties.


#### editor.restoreSelection()

> Restore the previously saved text ranges from the `editor.wysiwyg_range` and
> `editor.text_range` properties.


#### editor.setSelection(win, doc, node)

> Set the selection range to a specific element.
>
> Parameters:
>
> - **win** *{Window}* A reference to the window object.
> - **doc** *{Document}* A reference to the document object.
> - **node** *{Element}* A reference to the element that will be selected.


#### editor.action(cmd, val)

> Perform an action on the selected text.
>
> Parameters:
>
> - **cmd** *{String}* The action to perform.
> - **val** *[Mixed]* (Optional) Parameter to pass to the action.
>
> Example:
>
>     // Make selected text bold.
>     editor.action("bold");
>
> Available actions:
>> | Action             | Description                           |
>> | ------------------ | ------------------------------------- |
>> | **bold**           | Make selected text bold.              |
>> | **italic**         | Make selected text italic.            |
>> | **underline**      | Make selected text underlined.        |
>> | **strikethrough**  | Make selected text strike-through.    |
>> | **subscript**      | Make selected text subscript.         |
>> | **superscript**    | Make selected text superscript.       |
>> | **orderedlist**    | Insert a new ordered list.            |
>> | **unorderedlist**  | Insert a new unordered list.          |
>> | **outdent**        | Outdent (Un-indent) selected lines.   |
>> | **indent**         | Indent selected lines.                |
>> | **leftalign**      | Left-justify selected lines.          |
>> | **centeralign**    | Middle-justify selected lines.        |
>> | **rightalign**     | Right-justify selected lines.         |
>> | **blockjustify**   | Fill-justify selected lines.          |
>> | **undo**           | Undo last change.                     |
>> | **redo**           | Redo last change.                     |
>> | **hr**             | Insert a horizontal rule.             |
>> | **unlink**         | Remove a hyperlink.                   |
>> | **unformat**       | Remove all styles from selected text. |
>> | **print**          | Print WYSIWYG content.                |


#### editor.insert(cmd, msg, entry)

> Insert a resource, replacing any selected text.
>
> Parameters:
>
> - **cmd** *{String}* The action to perform.
> - **msg** *{String}* The prompt message to display to the user.
> - **entry** *[String]* (Optional) The default entry string.
>
> Example:
>
>     // Prompt to add an image, defaulting to the Google logo.
>     editor.insert("image", "Enter an image URL", "https://www.google.com/images/srpr/logo3w.png");
>
> Available actions:
>> | Action             | Description                           |
>> | ------------------ | ------------------------------------- |
>> | **image**          | Insert an image.                      |
>> | **link**           | Insert a hyperlink.                   |


#### editor.toggle(button)

> Toggle between WYSIWYG and Source mode.
>
> Parameters:
>
> - **button** *{Element}* The toggle button element (for swapping its text).
> The button can be referenced using `editor.toggler`
>
> Example:
>
>     editor.toggle(editor.toggler);


#### editor.post()

> Copy WYSIWYG content to the textarea element, accessible through `editor.el`.
>
> Example:
>
>     editor.post();
>     alert(editor.el.value);



### Properties

#### editor.wysiwyg_range

> *{Range}*
>
> Selection range for the WYSIWYG editor. This property is updated only when
> `editor.saveSelection()` is called.


#### editor.text_range

> *{Range}*
>
> Selection range for the text editor. This property is updated only when
> `editor.saveSelection()` is called.

#### editor.toggler

> *{Element}*
>
> A reference to the toggle button element.


#### editor.window

> *{Window}*
>
> A reference to the WYSIWYG window.


#### editor.document

> *{Document}*
>
> A reference to the WYSIWYG document.


#### editor.el

> *{Element}*
>
> A reference to the textarea element.


#### editor.iframe

> *{Element}*
>
> A reference to the iframe which contains the WYSIWYG window.


Minifying
---------

Install uglify-js:

    npm install uglify-js

Minify:

    uglify-js -o ueditor/ueditor.min.js ueditor.js
